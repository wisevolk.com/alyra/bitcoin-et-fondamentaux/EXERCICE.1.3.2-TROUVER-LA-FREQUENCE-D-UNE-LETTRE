## Exercice 1.3.2 : Trouver la fréquence d'une lettre dans un texte
Ecrire une fonction qui prend en entrée un texte et retourne le nombre d'occurrences de chaque caractère dans le texte.

frequences(“Etre contesté, c’est être constaté”)

        ==> t: 7, r: 2, e: 4, c: 3, o: 2, n: 2, s: 3, 'é': 2, 'ê': 1, a: 1
