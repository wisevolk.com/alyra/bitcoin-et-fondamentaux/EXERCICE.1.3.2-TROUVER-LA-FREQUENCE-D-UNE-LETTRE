function frequences(text) {
    const lettersMap = new Map();
    for ( let char of text ) {
        const pattern = "[0-9a-z\u00C0-\u024F]";
        if ( char.match(pattern) ) {
            if ( !lettersMap[char] ) {
                lettersMap[char] = 0;
            }
            lettersMap[char] += 1;
        }
    }
    return lettersMap;
}

console.log(frequences("Etre contesté, c’est être constaté"));
